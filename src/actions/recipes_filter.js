import { SEARCH_CHANGE, CATEGORY_FILTER_CHANGED } from './types';

export function searchChanged(term) {
  return function(dispatch) {
    dispatch({
      type: SEARCH_CHANGE,
      payload: term
    });
  }
}

export function categoryFilterChanged(term) {
  return function(dispatch) {
    dispatch({
      type: CATEGORY_FILTER_CHANGED,
      payload: term
    });
  }
}
