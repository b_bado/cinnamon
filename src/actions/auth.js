import { databaseRef, firebaseAuth } from '../config/firebase_config';
import { FAILED_LOGIN, IS_AUTHED, SIGN_OUT, NEW_USER } from './types';

var Calendars = databaseRef.child("calendars");

export function auth(email, pw, callback) {
	 return dispatch => {
      firebaseAuth.createUserWithEmailAndPassword(email, pw).then((snapshot) => {
        Calendars.push({user: snapshot.email, recipes: []});
        snapshot.sendEmailVerification().then(() => callback(), (error) => {
          console.log(error.message);
        });
      },(error) => {
        console.log(error.message);
      });
    }
}

export function sendEmailVerification() {
	return dispatch => {
		firebaseAuth.currentUser.sendEmailVerification();
	}
}

export function logout() {
  return dispatch => {
    firebaseAuth.signOut().then((snapshot) => {
      dispatch({
        type: SIGN_OUT,
        payload: snapshot
      });
    });
  }
}

export function isAuthed() {
  return dispatch => {
    firebaseAuth.onAuthStateChanged((user) => {
      if (user) {
        dispatch({
          type: IS_AUTHED,
          payload: user
        });
      } else {
        dispatch({
          type: NEW_USER,
          payload: true
        });
      }
    });
  }
}

export function login (email, pw) {
  return dispatch => {
    firebaseAuth.signInWithEmailAndPassword(email, pw).catch((error) => {
      dispatch({
        type: FAILED_LOGIN,
        payload: error
      });
    });
  }
}
