import _ from 'lodash';
import {databaseRef, storageRef} from '../config/firebase_config';
import {FETCH_RECIPES, FETCH_CATEGORIES, FETCH_RECIPE, OPEN_DELETE_MODAL, FETCH_CALENDAR_RECIPES, OPEN_CALENDAR_RECIPES_ADD} from './types';
import {reset} from 'redux-form';

var Recipes = databaseRef.child("recipes");
var Categories = databaseRef.child("categories");
var Calendars = databaseRef.child("calendars");

export function fetchRecipes(user) {
	return dispatch => {
		Recipes.orderByChild("user").equalTo(user).on('value', snapshot => {
			dispatch({
				type: FETCH_RECIPES,
				payload: snapshot.val()
			});
		}, error => {
			console.log(error);
		});
	};
}

export function fetchRecipe(recideId) {
	return dispatch => {
		databaseRef.child(`recipes/${recideId}`).on('value', snapshot => {
			dispatch({
				type: FETCH_RECIPE,
				payload: snapshot.val()
			});
		}, error => {
			console.log(error);
		});
	}
}

export function deleteRecipe(key) {
	return dispatch => Recipes.child(key).remove();
}

export function createRecipe(recipe, user) {
	return dispatch => {
		recipe.user = user;
		if (recipe.image) {
			let file = recipe.image.item(0);
			let imageRef = storageRef.child(`images/${file.name}`);
			imageRef.put(file).then((snapshot) => {
						recipe.image = snapshot.downloadURL;
						Recipes.push(recipe);
			});
		} else {
			recipe.image = "https://firebasestorage.googleapis.com/v0/b/cinnamon-2b7dc.appspot.com/o/images%2Ffood-profile_image-0a2c3ed2f8d119ea-300x300.png?alt=media&token=cf6c8719-ad43-4250-b08a-cd8db7a00d7d";
			Recipes.push(recipe);
		}
	}
}

export function updateRecipe(recipe, recipeId, mail, callback) {
	return dispatch => {
		let recipeRef = databaseRef.child(`recipes/${recipeId}`);
		// If new image was not uploaded
		if(typeof recipe.image === 'string') {
			setRecipeValues(recipeRef, recipe, callback);
		// If image was uploaded upload the image and then update existing recipe
		} else {
			let file = recipe.image.item(0);
			let imageRef = storageRef.child(`images/${file.name}`);
			imageRef.put(file).then((snapshot) => {
				recipe.image = snapshot.downloadURL;
				setRecipeValues(recipeRef, recipe, callback);
			});
		}
		// Update titles also in the calendar
		Calendars.orderByChild('user').equalTo(mail).once('value').then((snapshot) => {
			let recipeKey = _.findKey(snapshot.exportVal(), {user: mail});
			Calendars.child(recipeKey + '/recipes/').orderByChild('id').equalTo(recipeId).once('value').then(snapshot => {
				_.map(snapshot.val(), (meal, key) => {
					meal.title = recipe.title;
					snapshot.ref.child(key).set(meal);
				});
			});
		});
	}
}

export function fetchRecipeCategories() {
	return dispatch => {
		Categories.on('value', snapshot => {
			dispatch({
				type: FETCH_CATEGORIES,
				payload: snapshot.val()
			});
		}, error => {
			console.log(error);
		})
	}
}

export function openDeleteModal(callback) {
	return dispatch => {
		dispatch({
			type: OPEN_DELETE_MODAL,
			payload: callback
		})
	}
}

const setRecipeValues = (recipeRef, recipe, callback) => {
	recipeRef.set({
	  category : recipe.category,
    cookTime : recipe.cookTime,
    description : recipe.description,
    directions : recipe.directions,
    image : recipe.image,
    ingredients : recipe.ingredients,
    prepTime : recipe.prepTime,
    serves : recipe.serves,
    title : recipe.title,
		user: recipe.user
	}).then(() => callback());
}

export function fetchCalendarRecipes(mail) {
	return dispatch => {
		Calendars.orderByChild('user').equalTo(mail).on('value', snapshot => {
			let recipes;
			_.map(snapshot.val(), (val, key) => {
				recipes = val.recipes;
			});
			dispatch({
				type: FETCH_CALENDAR_RECIPES,
				payload: _.values(recipes)
			});
		});
	}
}

export function openCalendarAddModal(date) {
	return dispatch => {
		dispatch({
			type: OPEN_CALENDAR_RECIPES_ADD,
			payload: date
		});
		$('#addCalendarRecipeModal').modal();
	}
}

export function addRecipesToCalendar(recipes, mail, date) {
	return dispatch => {
		let ref;
		_.map(recipes, (recipe, key) => {
			Calendars.orderByChild('user').equalTo(mail).once('value').then((snapshot) => {
				let recipeKey = _.findKey(snapshot.exportVal(), {user: mail});
				let mealRef = Calendars.child(recipeKey + '/recipes').push();
				mealRef.set({
					id: key,
					title: recipe.title,
					start: date.format(),
					mealId: mealRef.key
				});
			});
		});
	}
}

export function removeRecipeFromCalendar(mail, id) {
	return dispatch => {
		Calendars.orderByChild('user').equalTo(mail).once('value').then((snapshot) => {
			let recipeKey = _.findKey(snapshot.exportVal(), {user: mail});
			Calendars.child(recipeKey + '/recipes/' + id).remove();
		});
	}
}
