import { FETCH_CALENDAR_RECIPES, OPEN_CALENDAR_RECIPES_ADD } from '../actions/types';

export default function(state = {recipes: [], date: ''}, action) {
	switch(action.type) {
		case FETCH_CALENDAR_RECIPES:
			return {...state, recipes: action.payload};
		case OPEN_CALENDAR_RECIPES_ADD:
			return {...state, date: action.payload};
	}
	return state;
}
