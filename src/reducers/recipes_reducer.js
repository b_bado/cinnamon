import _ from 'lodash';
import {FETCH_RECIPES, FETCH_RECIPE, DELETE_RECIPE, CREATE_RECIPE, OPEN_DELETE_MODAL} from '../actions/types';

export default function(state = {all:[], recipe: null, deleteModalOpen: false, deleteModalCallback: null}, action) {
	switch(action.type) {
		case FETCH_RECIPES:
			return {...state, all: action.payload};
		case DELETE_RECIPE:
			return _.omit(state, action.payload);
		case CREATE_RECIPE:
			return {...state, ...action.payload};
		case FETCH_RECIPE:
			return {...state, recipe: action.payload};
		case OPEN_DELETE_MODAL:
			return {...state, deleteModalOpen: true, deleteModalCallback: action.payload};
	}
	return state;
}