import { FAILED_LOGIN, SIGN_OUT, IS_AUTHED, NEW_USER } from '../actions/types';

export default function(state = {user: null, error: null, newUser: null}, action) {
	switch(action.type) {
		case FAILED_LOGIN:
			return {...state, error: action.payload};
		case IS_AUTHED:
			return {...state, user: action.payload};
		case SIGN_OUT:
			return {...state, error: null, user: null};	
		case NEW_USER:
			return {...state, newUser: action.payload};
	}
	return state;
}