import { combineReducers } from 'redux';
import {reducer as formReducer} from 'redux-form';
import recipesReducer from './recipes_reducer';
import categoriesReducer from './categories_reducer';
import authReducer from './auth_reducer';
import recipes_filter_reducer from './recipes_filter_reducer';
import calendarRecipesReducer from './calendar_recipes_reducer';

const rootReducer = combineReducers({
  recipes : recipesReducer,
  categories: categoriesReducer,
  form: formReducer,
  auth: authReducer,
  recipesFilter: recipes_filter_reducer,
  calendarRecipes: calendarRecipesReducer
});

export default rootReducer;
