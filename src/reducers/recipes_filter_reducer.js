import { SEARCH_CHANGE, CATEGORY_FILTER_CHANGED } from '../actions/types';

export default function(state = {searchValue: '', categoryFilterValue: ''}, action) {
	switch(action.type) {
		case SEARCH_CHANGE:
			return {...state, searchValue: action.payload};
		case CATEGORY_FILTER_CHANGED:
			return {...state, categoryFilterValue: action.payload};
	}
	return state;
}
