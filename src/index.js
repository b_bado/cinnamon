import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import reduxThunk from 'redux-thunk';
import reducers from './reducers';
import injectTapEventPlugin from 'react-tap-event-plugin';
import App from './components/app';
import isAuthed from './actions/auth';

const createStoreWithMiddleware = applyMiddleware(reduxThunk)(createStore);
injectTapEventPlugin();
ReactDOM.render(
  <Provider store={createStoreWithMiddleware(reducers)}>
  	<App />
  </Provider>
  , document.querySelector('.container-fluid'));
