import * as firebase from "firebase";

export const FirebaseConfig = require("./keys");

firebase.initializeApp(FirebaseConfig);

export const databaseRef = firebase.database().ref()
export const storageRef = firebase.storage().ref()
export const firebaseAuth = firebase.auth();
