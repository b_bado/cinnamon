import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import NavLink from '../wrappers/nav_link';
import * as actions from '../../actions/auth';

class Navigation extends Component {

  renderNavigation() {
    if (this.props.user && this.props.user.email) {
      return(
        <div className="collapse navbar-collapse" id="navbarNav">
          <ul className="navbar-nav mr-auto">
            <li className="nav-item px-1">
              <NavLink to="/recipes"><i className="fa fa-cutlery" aria-hidden="true"></i> My Recipes</NavLink>
            </li>
            <li className="nav-item px-1">
              <NavLink to="/recipes/calendar"><i className="fa fa-calendar" aria-hidden="true"></i> My Calendar</NavLink>
            </li>
            <li className="nav-item px-1">
              <NavLink to="/recipes/new"><i className="fa fa-plus-circle" aria-hidden="true"></i> Add Recipe</NavLink>
            </li>
          </ul>
          <ul className="navbar-nav ml-auto">
            <li className="nav-item">
              <span className="navbar-text px-2">
                <i className="fa fa-user" aria-hidden="true"></i>
                {' ' + this.props.user.email.substring(0,this.props.user.email.indexOf('@'))}
              </span>
            </li>
            <li className="nav-item">
              <NavLink to="/signout"><i className="fa fa-sign-out" aria-hidden="true"></i> Sign out</NavLink>
            </li>
          </ul>
      </div>
      );
    } else {
      return(
        <div className="collapse navbar-collapse" id="navbarNav">
          <ul className="navbar-nav ml-auto">
            <li className="nav-item px-1">
              <NavLink to="/register"><i className="fa fa-user-plus" aria-hidden="true"></i> Sign up</NavLink>
            </li>
            <li className="nav-item px-1">
              <NavLink to="/login"><i className="fa fa-sign-in" aria-hidden="true"></i> Sign in</NavLink>
            </li>
          </ul>
      </div>
      );
    }
  }

  render() {
    return(
      <nav className="navbar navbar-expand-lg fixed-top navbar-light bg-light">
        <NavLink className="navbar-brand" to="/recipes"><i className="fa fa-codiepie" aria-hidden="true"></i> Cinnamon</NavLink>
        <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        {this.renderNavigation()}
      </nav>
    );
  }
}

function mapStateToProps(state) {
  return {
    user: state.auth.user
  }
}

export default connect(mapStateToProps, actions)(Navigation);
