import React, {Component} from 'react';
import { Field, reduxForm } from 'redux-form';
import * as actions from '../../actions/auth';
import { connect } from 'react-redux';
import * as renderFunctions from './render_form_field';

class Register extends Component {
	onSubmit(values) {
		this.props.auth(values.email, values.password, () => {
			this.props.history.push("/verifyMail");
		});
	}

	render() {
		const { handleSubmit } = this.props;
		return(
			<div className="row">				
				<div className="col-sm-6 col-md-4 col-md-offset-4 col-centered loginForm">
					<div className="account-wall">
						<img className="profile-img" src="//ssl.gstatic.com/accounts/ui/avatar_2x.png" alt="" />
						<form className="form-signin" onSubmit={handleSubmit(this.onSubmit.bind(this))}>
							<Field name="email" type="text" placeholder="Email" component={renderFunctions.renderField}/>
							<Field name="password" type="password" placeholder="Password" component={renderFunctions.renderField}/>
							<Field name="passwordAgain" type="password" placeholder="Confirm password" component={renderFunctions.renderField}/>
							<button className="btn btn-lg btn-primary btn-block sign-form-submit" type="submit">Sign up</button>
						</form>
					</div>
				</div>
			</div>
		);
	}
}

function validate(values) {
	const errors = {};

	if (values.password && values.password.length < 6) {
		errors.password = "Password have to be at least 6 characters long"
	}

	if (values.password !== values.passwordAgain){
		errors.passwordAgain = "Passwords have to match"
	}

	if (!values.email) {
		errors.email = "Email is required";
	}

	if (values.email && !validateEmail(values.email)) {
		errors.email = "Enter valid email address";
	}

	if (!values.password) {
		errors.password = "Password is required";
	}
	return errors;
}

function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

export default reduxForm({
	validate,
	form: 'RegisterForm'
})(
	connect(null, actions)(Register)
);
