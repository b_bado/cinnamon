import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from '../../actions/auth';

class VerifyMail extends Component {

  constructor(props) {
    super(props);

    this.state = {
      emailSent: false
    }
  }

  handleClick() {
    this.props.sendEmailVerification();
    this.setState({emailSent: true});
  }

  renderResend() {
    if (!this.state.emailSent) {
      return <p>Did not receive the email? Click <b onClick={this.handleClick.bind(this)}>here</b></p>;
    } else {
      return <p>Email including activation link was sent again. <b>Check your inbox</b></p>;
    }
  }

  render() {
    return(
      <div className="row" style={{margin: "15px"}}>
        <div className="col-lg-12" style={{textAlign: "center", marginTop: '16%'}}>
          <img src="/style/icons/email.svg" style={{width: 200}}/>
          <br/>
          <h2>Your email was not verified yet</h2>
          <p>Email including activation link was sent to your email address. Please verify your email</p>
          {this.renderResend.call(this)}
        </div>
      </div>
    );
  }
}

function mapStateToProps(state){
  return {
    user: state.auth.user
  }
}

export default connect(null, actions)(VerifyMail);
