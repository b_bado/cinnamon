import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

export default function(ComposedComponent) {
  class Authentication extends Component {
    static contextTypes = {
      router: PropTypes.object
    }

    componentWillMount() {
      if (!this.props.authenticated) {
        this.context.router.history.push('/login');
      } else if (!this.props.emailVerified) {
        this.context.router.history.push('/verifyMail');
      }
    }

    componentWillUpdate(nextProps) {
      if (!nextProps.authenticated) {
        this.context.router.history.push('/login');
      } else if (!nextProps.emailVerified) {
        this.context.router.history.push('/verifyMail');
      }
    }

    render() {
      return <ComposedComponent {...this.props } />
    }
  }

  function mapStateToProps(state) {
    if (state.auth.user === null) {
      return { authenticated: false }
    } else {
      return {
        authenticated: true,
        emailVerified: state.auth.user.emailVerified
      }
    }
  }

  return connect(mapStateToProps)(Authentication);
}
