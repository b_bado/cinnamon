import React, {Component} from 'react';
import { Field, reduxForm } from 'redux-form';
import * as actions from '../../actions/auth';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import * as renderFunctions from './render_form_field';
import PropTypes from 'prop-types';

class Login extends Component {
	static contextTypes = {
		router: PropTypes.object
	}

	onSubmit(values) {
		this.props.login(values.email, values.password);
	}

	componentWillUpdate(nextProps) {
		if (nextProps.user && nextProps.user.email) {
			this.context.router.history.push('/recipes');
		}
	}

	componentWillMount() {
		if (this.props.user && this.props.user.email) {
			this.context.router.history.push('/recipes');
		}
	}

	render() {
		const { handleSubmit, user } = this.props;

		let loginProblem = this.props.error ? true : false
		return (
			<div className="row">
				<div className="col-sm-6 col-md-4 col-md-offset-4 col-centered loginForm">
					<div className="account-wall">
						<img className="profile-img" src="//ssl.gstatic.com/accounts/ui/avatar_2x.png" alt="" />
						<form className="form-signin" onSubmit={handleSubmit(this.onSubmit.bind(this))}>
							<Field name="email" type="text" placeholder="Email" component={renderFunctions.renderField}/>
							<Field name="password" type="password" placeholder="Password" component={renderFunctions.renderField}/>
							<button className="btn btn-lg btn-primary btn-block sign-form-submit" type="submit">Sign in</button>
						</form>
					</div>
					<Link to="/register" className="text-center new-account">Create an account</Link>
				</div>
			</div>
		);
	}
}

function validate(values) {
	const errors = {};

	if (!values.email) {
		errors.email = "Email is required";
	}

	if (values.email && !validateEmail(values.email)) {
		errors.email = "Enter valid email address";
	}

	if (!values.password) {
		errors.password = "Password is required";
	}

	return errors;
}

function mapStateToProps(state) {
	return {
		user: state.auth.user,
		error: state.auth.error
	}
}

function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

export default reduxForm({
	validate,
	form: 'LoginForm'
})(
	connect(mapStateToProps, actions)(Login)
);
