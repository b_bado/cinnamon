import React from 'react';
import _ from 'lodash';

export function renderField(field) {
  const { meta: {touched, error} } = field;
  if (touched && error) {
    return (
      <div>
        {field.label ? <label htmlFor={field.name} className="col-form-label">{field.label}</label> : null}
        <input {...field.input} id={field.name} type={field.type} className="form-control is-invalid" placeholder={field.placeholder} />
        <div className="invalid-feedback">
          {error}
        </div>
      </div>
    );
  }

  return(
    <div>
      {field.label ? <label htmlFor={field.name} className="col-form-label">{field.label}</label> : null}
      <input {...field.input} type={field.type} className="form-control" placeholder={field.placeholder} />
    </div>
  );
}

export function renderTextArea(field) {
  const { meta: {touched, error} } = field;
  if (touched && error) {
    return (
      <div>
        {field.label ? <label htmlFor={field.name} className="col-form-label">{field.label}</label> : null}
        <textarea {...field.input} id={field.name} type={field.type} className="form-control is-invalid" placeholder={field.placeholder} rows={field.rows} />
        <div className="invalid-feedback">
          {error}
        </div>
      </div>
    );
  }

  return(
    <div>
      {field.label ? <label htmlFor={field.name} className="col-form-label">{field.label}</label> : null}
      <textarea {...field.input} id={field.name} type={field.type} className="form-control" placeholder={field.placeholder} rows={field.rows}/>
    </div>
  );
}

export function renderSelectField(field) {
  return(
    <div>
      {field.label ? <label htmlFor={field.name} className="col-form-label">{field.label}</label> : null}
      <select {...field.input} id={field.name} className="form-control" >
        {_.map(field.options, option => <option key={option.name}>{option.name}</option>)}
      </select>
    </div>
  )
}
