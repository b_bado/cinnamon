import React, { Component } from 'react';
import * as actions from '../../actions/auth';
import { connect } from 'react-redux';

class SignOut extends Component {
  componentWillMount() {
    this.props.logout();
  }

  render() {
    return(
      <div className="row" style={{margin: "15px"}}>
        <div className="col-lg-12" style={{textAlign: "center", marginTop: '16%'}}>
          <img src="/style/icons/exit.svg" style={{width: 200}}/>
          <h2>Sorry to see you go!</h2>
          <p>Hope you will come back later</p>
        </div>
      </div>
    );
  }
}

export default connect(null, actions)(SignOut);
