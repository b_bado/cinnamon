import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

class NavLink extends React.Component {
    static contextTypes = {
      router: PropTypes.object
    }

    render() {
        let isActive = this.context.router.route.location.pathname === this.props.to;
        let className = isActive ? 'nav-link active' : 'nav-link';

        return(
            <Link className={className} {...this.props}>
                {this.props.children}
            </Link>
        );
    }
}

export default NavLink;
