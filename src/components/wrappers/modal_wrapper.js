import React, {Component} from 'react';
import { connect } from 'react-redux';

// Material UI
import DeleteForever from 'material-ui/svg-icons/action/delete-forever';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import IconButton from 'material-ui/IconButton';

class ModalWrapper extends Component {
	constructor(props) {
		super(props);
	}

	componentWillReceiveProps(nextProps) {
		if (true === nextProps.deleteModalOpen) {
			this.setState({open: true});
		}
	}

	state = {
		open: false
	};

	handleOpen = () => {
		this.setState({open: true});
	};

	handleClose = () => {
		this.setState({open: false});
	};

	handleSubmit = () => {
		this.props.deleteModalCallback();
		this.handleClose();
	}

	render() {
	    const actions = [
	      <FlatButton
	        label="Cancel"
	        primary={true}
	        onTouchTap={this.handleClose}
	      />,
	      <FlatButton
	        label="Submit"
	        primary={true}
	        onTouchTap={this.handleSubmit}
	      />,
	    ];

		return(
			<div>
	      <Dialog
	          title="Delete recipe"
	          actions={actions}
	          modal={false}
	          open={this.state.open}
	          onRequestClose={this.handleClose}
	        >
	          This recipe will be permanently deleted. Are you sure you want to continue?
	      </Dialog>
			</div>
		);
	}
}

function mapStateToProps(state) {
	return {
		deleteModalOpen: state.recipes.deleteModalOpen,
		deleteModalCallback: state.recipes.deleteModalCallback
	}
}

export default connect(mapStateToProps, null)(ModalWrapper);
