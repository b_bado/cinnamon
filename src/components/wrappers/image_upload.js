import React, {Component} from 'react';
import Dropzone from 'react-dropzone';

// Material UI
import AddCircleOutline from 'material-ui/svg-icons/content/add-circle-outline';


export default class ImageUpload extends Component {
	imgStyle = {
		border: ""
	}

	constructor(props) {
		super(props);

		let {initialValue} = this.props;

		if (initialValue) {
			this.state = {
				uploadedFile: {
					preview: initialValue
				}
			}
		} else {
			this.state = {
				uploadedFile: ''
			}
		}
	}

	onImageDrop(files) {
		this.setState({
			uploadedFile: files[0]
		});
	}

	render() {
		const { value, onChange } = this.props.input;
		return(
			<div style={imageUploadDivStyle}>
			    <Dropzone
			      onChange={onChange}
			      multiple={false}
			      style={dropzoneStyle}
			      accept="image/*"
			      onDrop={this.onImageDrop.bind(this)}>
			      <p style={{marginTop: "10px"}}>Drop an image or click to select a file to upload</p>
			    </Dropzone>
			    <br />
				<div>
        			{this.state.uploadedFile == '' ? null :
        				<div>
          				<img style={imagePreviewStyle} src={this.state.uploadedFile.preview} />
          				<p>{this.state.uploadedFile.name}</p>
        			</div>}
      			</div>
			</div>
		);
	}
}

const imagePreviewStyle = {
	border: "1px solid #ddd",
	borderRadius: "4px",
	padding: "5px",
	width: "150px"
}

const imageUploadDivStyle = {
	width: "100%",
	textAlign: "center",
}

const dropzoneStyle = {
	height: "45px",
	border: "2px dashed #8a8a8a",
	backgroundColor: "#f5f5f5"
}
