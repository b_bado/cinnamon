import React, {Component} from 'react';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';

export default class SelectFieldWrapper extends Component {

	onChange = (event, index, value) => {
		if (this.props.input.onChange) {
			this.props.input.onChange(value);
		}
	}

	renderSelectFields = () => {
		let count = 0;
		return _.map(this.props.fields, (field, key) => {
			count++;
			return <MenuItem value={field.name} key={key} primaryText={field.name} />
		});
	}

	render() {
		return (
	        <SelectField
		      floatingLabelText="Category"
		      {...this.props.input}
		      onChange={this.onChange.bind(this)}
		      children={this.renderSelectFields()}
		    >
		    </SelectField>
		);
	}
}