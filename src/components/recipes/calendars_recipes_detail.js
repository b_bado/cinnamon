import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from '../../actions';

class CalendarRecipesDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      today: new Date()
    }
  }

  componentDidUpdate() {
    if (this.props.modalId && this.props.recipe) {
      $('#' + this.props.modalId).modal();
    }
  }

  handleMealDelete() {
    this.props.removeRecipeFromCalendar(this.props.user, this.props.modalId);
    $(`#${this.props.modalId}`).modal('hide');
    this.props.history.push('calendar');
  }

  render() {
    let recipe = this.props.recipe;
    if (this.props.modalId && recipe) {
      return(
        <div className="modal" id={this.props.modalId}>
          <div className="modal-dialog" role="document">
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title">{recipe.title}</h5>
                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div className="modal-body">
                <div className="container-fluid">
                  <div className="row">
                    <div className="col-4">
                      <img className=".img-thumbnail" src={recipe.image} style={{width: '10rem'}}/>
                    </div>
                    <div className="col-1"></div>
                    <div className="col-7">
                      <b>Description</b>
                      <p>{recipe.description}</p>
                    </div>
                  </div>
                  <hr/>
                  <div className="row">
                    <div className="col-4">
                      <b>Prep Time</b>
                      <p>{recipe.prepTime}</p>
                    </div>
                    <div className="col-4">
                      <b>Cook Time</b>
                      <p>{recipe.cookTime}</p>
                    </div>
                    <div className="col-4">
                      <b>Serves</b>
                      <p>{recipe.serves}</p>
                    </div>
                  </div>
                  <hr/>
                  <div className="row">
                    <div className="col-12">
                      <b>Ingredients</b>
                      <p>{recipe.ingredients}</p>
                    </div>
                  </div>
                  <br/>
                  <div className="row">
                    <div className="col-12">
                      <b>Directions</b>
                      <p>{recipe.directions}</p>
                    </div>
                  </div>
                </div>
              </div>
              <div className="modal-footer">
                {this.props.startDate > this.state.today ? <button type="button" onClick={this.handleMealDelete.bind(this)} className="btn btn-danger"><i className="fa fa-trash" aria-hidden="true"></i> Delete</button> : null}
                <button type="button" className="btn btn-secondary" data-dismiss="modal"><i className="fa fa-ban" aria-hidden="true"></i> Close</button>
              </div>
            </div>
          </div>
        </div>
      );
    }
    return null;
  }
}

function mapStateToProps(state) {
  return {
    recipe: state.recipes.recipe,
    user: state.auth.user.email
  }
}

export default connect(mapStateToProps, actions)(CalendarRecipesDetail)
