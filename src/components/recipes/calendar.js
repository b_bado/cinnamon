import React, { Component } from 'react';
import { connect } from 'react-redux';
import fullCalendar from 'fullcalendar';
import CalendarRecipesIndex from './calendar_recipes_index';
import CalendarRecipesDetail from './calendars_recipes_detail';
import * as actions from '../../actions';
import _$ from 'jquery';

class Calendar extends Component {
  constructor(props){
    super(props);
    this.state = {
      clickedRecipeId: '',
      eventStartDate: '',
      today: new Date(),
      clickedPastDate: false
    }
  }

  componentDidMount() {
    if (this.props.user) {
      this.props.fetchCalendarRecipes(this.props.user.email);

        // Initialize calendar
      _$('#calendar').fullCalendar({
        header: {
          left: 'prev,next today',
          center: 'title',
          right: 'agendaWeek'
        },
        droppable: true,
        defaultView: 'agendaWeek',
        minTime: '06:00:00',
        maxTime: '23:00:00',
        allDaySlot: false,
        contentHeight: 'auto',
        firstDay: 1,
        dayClick: this.addMeal.bind(this),
        eventClick: this.handleEventClick.bind(this),
        forceEventDuration: true,
        defaultTimedEventDuration: "00:30:00",
        eventColor: '#B2EBF2',
        eventBorderColor: 'rgb(132, 227, 239)',
        displayEventTime: false,
        eventRender: this.renderEvent.bind(this)
      });
    }
  }

  componentWillUpdate(nextProps) {
    _$('#calendar').fullCalendar( 'removeEvents' );
    _$('#calendar').fullCalendar( 'renderEvents', nextProps.calendarRecipes, 'stick' );
  }

  renderEvent(event, element) {
    if (event.start.toDate() < this.state.today) {
      element.css("background-color", "rgba(117, 117, 117, 0.47)");
      element.css("border-color", "#949292");
    }
  }

  renderAlert(){
    if (this.state.clickedPastDate) {
      return(
        <div className="alert alert-warning fade show" role="alert">
          <i className="fa fa-exclamation-circle" aria-hidden="true"></i> Can't add meal to the past
        </div>
      );
    }
  }

  addMeal(date, jsEvent, view) {
    if (date > this.state.today) {
      this.props.openCalendarAddModal(date);
    } else {
      this.setState({clickedPastDate: true});
      setTimeout(() => this.setState({clickedPastDate: false}), 2000)
    }
  }

  handleEventClick(calEvent, jsEvent, view) {
    this.props.fetchRecipe(calEvent.id);
    this.setState({
      clickedRecipeId: calEvent.mealId,
      eventStartDate: calEvent.start.toDate()
    });
  }

  render() {
    return(
      <div className="row justify-content-lg-center">
        <div className="col-lg-8" style={{paddingTop: 15, marginTop: 20}}>
          {this.renderAlert()}
          <div id="calendar"></div>
        </div>
        <CalendarRecipesIndex history={this.props.history}/>
        <CalendarRecipesDetail history={this.props.history} modalId={this.state.clickedRecipeId} startDate={this.state.eventStartDate}/>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    user: state.auth.user,
    calendarRecipes: state.calendarRecipes.recipes
  }
}

export default connect(mapStateToProps, actions)(Calendar);
