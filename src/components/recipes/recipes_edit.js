import _ from 'lodash';
import React, {Component} from 'react';
import { connect } from 'react-redux';
import { Field, reduxForm } from 'redux-form';
import * as actions from '../../actions'
import * as renderFunctions from '../../components/auth/render_form_field';
import ImageUpload from '../wrappers/image_upload';
import { Link } from 'react-router-dom';

class RecipesEdit extends Component {

	componentWillMount() {
		this.props.fetchRecipeCategories();
	}

	onSubmit(values) {
		this.props.updateRecipe(values, this.props.match.params.id, this.props.user, () => {
			this.props.history.push('/recipes');
		});
	}

	render() {
		const { handleSubmit, recipe } = this.props;
		if (!recipe) {
			return <div>Loading...</div>
		}

		return(
			<div className="row justify-content-md-center">
				<div className="col-md-6 col-lg-4">
					<form onSubmit={handleSubmit(this.onSubmit.bind(this))} className="recipe-edit-form">
						<h2 className="text-xs-center text-md-center">Edit Recipe</h2>
						<hr></hr>
						<div className="form-group">
							<Field
								label="Title"
								name="title"
								component={renderFunctions.renderField}
							/>
						</div>
						<div className="form-group">
							<Field
								label="Description"
								name="description"
								rows="3"
								component={renderFunctions.renderTextArea}
							/>
						</div>
						<div className="form-row">
							<div className="col">
								<Field
									label="Prep Time (min)"
									name="prepTime"
									component={renderFunctions.renderField}
								/>
							</div>
							<div className="col">
								<Field
									label="Cook Time (min)"
									name="cookTime"
									component={renderFunctions.renderField}
								/>
							</div>
							<div className="col">
								<Field
									label="Serves"
									name="serves"
									component={renderFunctions.renderField}
								/>
							</div>
						</div>
						<div className="form-group">
							<Field
								label="Cateogry"
								name="category"
								component={renderFunctions.renderSelectField}
								options={this.props.categories}
							/>
						</div>
						<div className="form-group">
							<Field
								label="Ingredients"
								name="ingredients"
								rows={3}
								component={renderFunctions.renderTextArea}
							/>
						</div>
						<div className="form-group">
							<Field
								label="Directions"
								name="directions"
								rows={3}
								component={renderFunctions.renderTextArea}
							/>
						</div>
						<div className="form-group">
							<Field
								label="Image"
								name="image"
								component={ImageUpload}
							/>
						</div>
						<div className="form-row">
							<div className="col">
								<button style={{width: "100%"}} type="submit" className="btn btn-primary"><i className="fa fa-check-circle" aria-hidden="true"></i> Submit</button>
							</div>
							<div className="col">
								<Link to="/recipes" style={{width: "100%"}} className="btn btn-danger"><i className="fa fa-ban" aria-hidden="true"></i> Cancel</Link>
							</div>
						</div>
					</form>
				</div>
			</div>
    );
	}
}

function validate(values) {
	const errors = {};
	if (!values.title) {
		errors.title = "Title is required";
	}
	if (!values.description) {
		errors.description = "Description is required";
	}
	if (!values.prepTime) {
		errors.prepTime = "Preparation time is required";
	}
	if (!values.cookTime) {
		errors.cookTime = "Cooking time is required";
	}
	if (!values.ingredients) {
		errors.ingredients = "Ingredients are required";
	}
	if (!values.directions) {
		errors.directions = "Directions are required";

}
	return errors;
}

function mapStateToProps(state) {
	return {
		categories: state.categories,
		recipe: state.recipes.recipe,
		initialValues: state.recipes.recipe,
		user: state.auth.user.email
	};
}

RecipesEdit = reduxForm({
	validate,
	form: 'RecipesEditForm'
})(RecipesEdit)

RecipesEdit = connect(mapStateToProps,actions)(RecipesEdit)

export default RecipesEdit
