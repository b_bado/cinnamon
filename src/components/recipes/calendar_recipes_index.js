import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from '../../actions';

const CATEGORY_FILTER_VALUE = 'Filter by category';

class CalendarRecipesIndex extends Component {

  constructor(props) {
    super(props);

    this.state = {
      categoryFilterValue: '',
      selectedRecipes: {}
    }
  }

  componentWillMount() {
    if (this.props.user) {
      this.props.fetchRecipeCategories();
      this.props.fetchRecipes(this.props.user.email);
    }
  }

  handleCategoryFilterChange(term) {
      this.setState({categoryFilterValue: term});
  }

  addToSelected(recipe, key) {
    this.setState({selectedRecipes: {...this.state.selectedRecipes, [key]: recipe}});
  }

  removeFromSelected(key) {
    this.setState({selectedRecipes: _.omit(this.state.selectedRecipes, key)});
  }

  resetSelectedRecipes() {
    this.setState({selectedRecipes: {}});
  }

  addRecipesToCalendar() {
    this.props.addRecipesToCalendar(this.state.selectedRecipes, this.props.user.email, this.props.date);
    $('#addCalendarRecipeModal').modal('hide');
    this.resetSelectedRecipes();
    this.props.history.push('calendar');
  }

  renderSelectedRecipes() {
    return _.map(this.state.selectedRecipes, (recipe, key) => {
      return(
        <div key={key} className="col-5 calendar-recipe" style={{margin: 10, zIndex: 999}}>
          <button onClick={event => {this.removeFromSelected.bind(this); this.removeFromSelected(key)}} className="btn" style={{width: '100%', backgroundColor: '#B2EBF2'}}>{recipe.title}</button>
        </div>
      );
    });
  }

  renderRecipes() {
    let recipes = _.map(this.props.recipes, (recipe, key) => {
      if (_.includes(recipe.category, this.state.categoryFilterValue) && !_.includes(this.state.selectedRecipes, recipe)) {
        return(
          <div key={key} className="col-5 calendar-recipe" style={{margin: 10, zIndex: 999}}>
            <button onClick={event => {this.addToSelected.bind(this); this.addToSelected(recipe, key)}} className="btn" style={{width: '100%', backgroundColor: '#B2EBF2'}}>{recipe.title}</button>
          </div>
        );
      }
    });

    recipes = _.filter(recipes, (recipe) => {
      return recipe;
    });
    if (recipes.length) {
      return recipes;
    }

    return(
      <div className="col-12" style={{textAlign: "center"}}>
        <h3>No recipes found</h3>
      </div>
    );
  }

  render() {
    return(
      <div className="modal" id="addCalendarRecipeModal">
        <div className="modal-dialog" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title">Add Recipe</h5>
              <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div className="modal-body">
              <div className="row" style={{textAlign: "center"}}>
                <div className="col-12" style={{marginBottom: 20}}>
                  <select value={this.state.categoryFilterValue} onChange={event => this.handleCategoryFilterChange(event.target.value)} className="custom-select">
                    <option value=''>{CATEGORY_FILTER_VALUE}</option>
                    {_.map(this.props.categories, category => <option key={category.name} value={category.name}>{category.name}</option>)}
                  </select>
                </div>
                {this.renderRecipes()}
                {!_.isEmpty(this.state.selectedRecipes) ? <div className="col-12"><hr/></div> : null}
                {this.renderSelectedRecipes()}
              </div>
            </div>
            <div className="modal-footer">
              <button onClick={this.addRecipesToCalendar.bind(this)} type="button" className={_.isEmpty(this.state.selectedRecipes) ? "btn btn-primary disabled" : "btn btn-primary"}><i className="fa fa-check" aria-hidden="true"></i> Add Recipe</button>
              <button onClick={this.resetSelectedRecipes.bind(this)} type="button" className="btn btn-danger"><i className="fa fa-repeat" aria-hidden="true"></i> Reset</button>
              <button onClick={this.resetSelectedRecipes.bind(this)} type="button" className="btn btn-secondary" data-dismiss="modal"><i className="fa fa-ban" aria-hidden="true"></i> Close</button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    recipes: state.recipes.all,
    categories: state.categories,
    user: state.auth.user,
    date: state.calendarRecipes.date
  }
}

export default connect(mapStateToProps, actions)(CalendarRecipesIndex);
