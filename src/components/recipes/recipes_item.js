import React, {Component} from 'react';
import {GridTile} from 'material-ui/GridList';
import { connect } from 'react-redux';
import * as actions from '../../actions';
import ModalWrapper from '../wrappers/modal_wrapper'
import { Link } from 'react-router-dom';

class RecipesItem extends Component {

	handleEdit() {
		this.props.fetchRecipe(this.props.id);
		this.props.push(`/recipes/${this.props.id}/edit`);
	}

	handleRequestDelete() {
		this.props.openDeleteModal(() => {
			this.props.deleteRecipe(this.props.id);
		});
	}

	render() {
		const {recipe} = this.props;
		return(
			<div className="card" style={{width: "15rem", marginBottom: "10px"}}>
			  <img style={{height: "200px", width: "100%"}}className="card-img-top" src={recipe.image} alt="Recipe image"/>
			  <div className="card-body">
			    <h4 className="card-title">{recipe.title}</h4>
			    <p className="card-text">{recipe.description}</p>
					<button className="btn btn-primary" onClick={this.handleEdit.bind(this)}><i className="fa fa-pencil" aria-hidden="true"></i> Edit</button>
					<button className="btn btn-danger float-right" onClick={this.handleRequestDelete.bind(this)}><i className="fa fa-trash" aria-hidden="true"></i> Delete</button>
			  </div>
			</div>
		);
	}
}

export default connect(null, actions)(RecipesItem);
