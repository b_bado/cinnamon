import React, { Component} from 'react';
import { connect } from 'react-redux';
import * as actions from '../../actions/recipes_filter';

const CATEGORY_FILTER_VALUE = 'Filter by category';

class RecipesFilter extends Component {
  constructor(props) {
    super(props);

    this.state = {
      searchValue: '',
      categoryFilterValue: CATEGORY_FILTER_VALUE
    }

    this.search = _.debounce((term) => {
      this.props.searchChanged(term);
    }, 300);
  }

  componentWillMount() {
    if (this.props.recipesFilter.searchValue) {
      this.setState({searchValue: this.props.recipesFilter.searchValue});
    }
    if (this.props.recipesFilter.categoryFilterValue) {
      this.setState({categoryFilterValue: this.props.recipesFilter.categoryFilterValue});
    }
  }

  handleSearchChange(term) {
    this.setState({searchValue: term});
    this.search(term);
  }

  handleCategoryFilterChange(term) {
      this.setState({categoryFilterValue: term});
      this.props.categoryFilterChanged(term);
  }

  render() {
    return(
      <div className="col-12" style={{marginBottom: "15px"}}>
        <div className="row">
          <div className="col-lg-3 col-sm-6">
            <div className="input-group">
              <span className="input-group-addon" id="search-addon">Search</span>
              <input value={this.state.searchValue} onChange={event => this.handleSearchChange(event.target.value)} type="text" className="form-control" placeholder="Search for recipe" aria-label="recipe" aria-describedby="search-addon" />
            </div>
          </div>
          <div className="col-lg-3 col-sm-6">
            <select value={this.state.categoryFilterValue} onChange={event => this.handleCategoryFilterChange(event.target.value)} className="custom-select">
              <option value=''>{CATEGORY_FILTER_VALUE}</option>
              {_.map(this.props.categories, category => <option key={category.name} value={category.name}>{category.name}</option>)}
            </select>
          </div>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    recipesFilter: state.recipesFilter,
    categories: state.categories
  }
}


export default connect(mapStateToProps, actions)(RecipesFilter);
