import _ from 'lodash';
import React, {Component} from 'react';
import * as actions from '../../actions';
import RecipesItem from './recipes_item';
import ModalWrapper from '../wrappers/modal_wrapper';
import RecipesFilter from './recipes_filter';
import { connect } from 'react-redux';

class RecipesIndex extends Component {

	componentWillMount() {
		if (this.props.user) {
			this.props.fetchRecipeCategories();
			this.props.fetchRecipes(this.props.user.email);
		}
	}

	renderRecipes() {
		let recipes = _.map(this.props.recipes, (recipe, key) => {
			if (_.includes(recipe.title, this.props.recipesFilter.searchValue) && _.includes(recipe.category, this.props.recipesFilter.categoryFilterValue)) {
				return(
					<div className="col-xl-2 col-lg-3 col-md-4 col-sm-6 col-xs-12" key={key}>
						<RecipesItem {...this.props.history} key={key} recipe={recipe} id={key} />
					</div>
				);
			}
		});
		recipes = _.filter(recipes, (recipe) => {
			return recipe;
		});
		if (recipes.length) {
			return recipes;
		}
		return(
			<div className="col-lg-12" style={{textAlign: "center"}}>
				<h3>No recipes found</h3>
			</div>
		);
	}

	render() {
		return (
			<div className="row" style={{margin: "15px"}}>
				<RecipesFilter />
				{this.renderRecipes()}
				<ModalWrapper />
			</div>
		);
	}
}

function mapStateToProps(state) {
	return {
		recipesFilter: state.recipesFilter,
		recipes: state.recipes.all,
		user: state.auth.user
	};
}

export default connect(mapStateToProps, actions)(RecipesIndex);
