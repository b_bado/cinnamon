import React, {Component} from 'react';
import injectTapEventPlugin from 'react-tap-event-plugin';
import requireAuth from './auth/require_auth';
import { BrowserRouter, Route,  Switch, Redirect } from 'react-router-dom'
import RecipesIndex from './recipes/recipes_index';
import RecipesEdit from './recipes/recipes_edit';
import RecipesNew from './recipes/recipes_new';
import Calendar from './recipes/calendar';
import Register from './auth/register';
import Login from './auth/login';
import SignOut from './auth/sign_out';
import Welcome from './welcome';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import AppBar from 'material-ui/AppBar';
import Navigation from './common/navigation';
import VerifyMail from './auth/verify_mail';
import { browserHistory } from 'react-router'
import { connect } from 'react-redux';
import * as actions from '../actions/auth';

// Material UI
import CircularProgress from 'material-ui/CircularProgress';

class App extends Component {
	constructor(props) {
		super(props);

		this.state = {
			loadingDone: false
		}

		props.isAuthed();
	}

	isAuthed() {
		return (this.props.user && this.props.user.email) ? true : false
	}

	componentWillReceiveProps() {
		this.setState({
			loadingDone: true
		});
	}

	render() {
		if (!this.state.loadingDone) {
			return (
				<MuiThemeProvider>
					<CircularProgress size={60} thickness={7} />
				</MuiThemeProvider>
			);
		}

		return(
			<MuiThemeProvider>
					<BrowserRouter>
						<div>
							<Route path="/" component={Navigation} />
							<Redirect path="/" to="/recipes"/>
							<Switch>
								<Route path="/recipes/calendar" component={requireAuth(Calendar)}/>
								<Route path="/recipes/new" component={requireAuth(RecipesNew)}/>
								<Route path="/recipes/:id/edit" component={requireAuth(RecipesEdit)}/>
								<Route path="/recipes" component={requireAuth(RecipesIndex)}/>
							</Switch>
					  	<Switch>
								<Route path="/register" component={Register} />
								<Route path="/login" component={Login}/>
								<Route path="/signout" component={SignOut}/>
								<Route path="/verifyMail" component={VerifyMail}/>
							</Switch>
						</div>
		    	</BrowserRouter>
	    	</MuiThemeProvider>
		);
	}
}

function mapStateToProps(state) {
	return {
		user: state.auth.user,
		newUser: state.auth.newUser
	}
}

export default connect(mapStateToProps, actions)(App);
